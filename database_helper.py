'''
Created on 2 Feb 2018

@author: Özgür Özer
'''

import os
import sqlite3

from flask import g

dbPath = os.getcwd() + "/database.db"


def connect_db():
    '''Connects to the db'''
    rv = sqlite3.connect(dbPath)
    rv.row_factory = sqlite3.Row
    return rv


def get_db():
    "Opens a new database if none exists for the current application"
    if not hasattr(g, 'db'):
        g.db = connect_db()
    return g.db


def close_db(error):
    '''Closes the database again at the end of the request'''
    if error:
        return error
    if hasattr(g, 'db') and not g.db.in_transaction:
        g.db.commit(); g.db.close(); del g.db


def insert_account(email, firstname, familyname, gender, city, country,
                   password):
    with get_db() as con:
        con.execute("INSERT INTO \
     accounts(email, firstname, familyname, gender, city, country, password) \
      VALUES(?,?,?,?,?,?,?)",
                    (email, firstname, familyname, gender, city, country,
                     password))


def get_account_by_email(email):
    with get_db() as con:
        cur = con.execute("SELECT * FROM accounts WHERE email = ?;", (email,))
        return cur.fetchone()


def get_account_by_token(token):
    with get_db() as con:
        cur = con.execute("SELECT * FROM accounts WHERE token= ?;", (token,))
        return cur.fetchone()


def change_password(password, token):
    with get_db() as con:
        con.execute("UPDATE accounts SET password = ? WHERE token = ?;",
                    (password, token))


def add_message(msg, email):
    with get_db() as con:
        con.execute("UPDATE accounts SET messages = ? WHERE email = ?",
                    (msg, email))


def insert_token(token, email):
    with get_db() as con:
        con.execute("UPDATE accounts SET token = ? WHERE email = ?",
                    (token, email))


def user_count():
    with get_db() as con:
        return tuple(con.execute("SELECT Count(*) FROM accounts").fetchone())[0]


def add_image(image, email):
    with get_db() as con:
        con.execute("UPDATE accounts SET images = ? WHERE email = ?",
                    (image, email))


def add_video(video, email):
    with get_db() as con:
        con.execute("UPDATE accounts SET videos = ? WHERE email = ?",
                    (video, email))

