"""
Server implementation for twidder SPA application using Flask framework.

Created on 2 Feb 2018
@author: Özgür Özer
Implemented Features:
-Live data representation
-HTML drag and drop
-History API
-Further security measures
-Media streaming

Modules:
-database_helper: All database interactions are here

Every function except for get_member_count, sign in, sign up, and sign out
validates input by hash. Private key is the token and public key is the email.
Hashing salts are given in the functions in the function parameters of
controlHash.update
"""

import json
import os
from hashlib import sha512
from random import choices
from string import ascii_letters

from flask import Flask, jsonify, request, send_from_directory
from flask_bcrypt import Bcrypt
from flask_sockets import Sockets
from gevent.pywsgi import WSGIServer
from geventwebsocket.handler import WebSocketHandler
from werkzeug.utils import secure_filename

import database_helper

app = Flask(__name__)
sockets = Sockets(app)
bcrypt = Bcrypt(app)

dir = os.getcwd()
app.config["UPLOAD_FOLDER"] = dir + "/static/media"

# Contains the web sockets during execution. Every email is assigned to a ws
TwidderSessions = dict()


@sockets.route("/echo")
def socket(ws):
    """Maintain web sockets and single login interface"""
    while not ws.closed:
        data = ws.receive()
        # Web socket some times sends nothing. This is to prevent error
        if data:
            data = json.loads(data)
            Hash = data["hash"]
            email = data["email"]
            entry = database_helper.get_account_by_email(email)
            if entry:
                controlHash = sha512()
                controlHash.update(bytes(entry["token"] + email, "utf-8"))
                controlHash = controlHash.hexdigest()
                if controlHash == Hash:
                    # This is single login interface implementation.
                    # If user has signed in in another browser before
                    # remove the previous one, send the signal "newConnection"
                    # causing it to sign out.
                    update_ws()
                    if email in TwidderSessions:
                        preSes = TwidderSessions.pop(email)
                        preSes.send(json.dumps("newConnection"))
                        preSes.close()
                    TwidderSessions.update({email: ws})
                    update_graph()
                else:
                    print("hash mismatch")
            else:
                print("No such email as", email)


@app.teardown_appcontext
def teardown(error):
    database_helper.close_db(error)


@app.route("/")
@app.route("/browse")
@app.route("/home")
@app.route("/account")
def welcome():
    return app.send_static_file('client.html')


@app.route('/sign_in', methods=["PUT", "GET"])
def sign_in():
    # inpt: email, password
    inpt = request.get_json()
    email, password = inpt["email"], inpt["password"]
    entry = database_helper.get_account_by_email(email)
    if not entry:
        return "Email not found", 401
    if bcrypt.check_password_hash(entry["password"], password):
        # By this loop the possibility a non-unique token is generated is
        # mitigated. The loop exits only if token is not in the database already
        while True:
            token = token_generator()
            if not database_helper.get_account_by_token(token):
                break
        database_helper.insert_token(token, email)
        update_graph()
        return jsonify(token)
    return "Wrong password", 401


@app.route("/sign_up", methods=["PUT"])
def sign_up():
    # data: email, first name, family name, password, city, country
    data = request.get_json()
    for ent in data:
        if (ent != "token" or ent != "messages") and not data[ent]:
            return ent + " cannot be empty", 422
    entry = database_helper.get_account_by_email(data["email"])
    if entry:
        return "This email belongs to another user", 401
    # Store user password hashed
    data['password'] = bcrypt.generate_password_hash(
        data["password"]).decode("utf-8")
    database_helper.insert_account(**data)
    return "Sign up successful", 200


@app.route("/sign_out", methods=["POST"])
def sign_out():
    # data: email, hash
    email = request.get_json()
    entry = database_helper.get_account_by_email(email)
    if not entry:
        return "No such user", 401
    database_helper.insert_token("", entry["email"])
    update_graph()
    return "Successfully signed out", 200


@app.route("/change_password", methods=["POST"])
def change_password():
    # data: email, hash, old password, new password
    data = request.get_json()
    email = data["email"]
    Hash = data["hash"]
    entry = database_helper.get_account_by_email(email)
    if not entry:
        return "User not found", 404
    controlHash = sha512()
    controlHash.update(bytes(
        entry["token"] + email + data["oldPassword"] + data["newPassword"],
        "utf-8"))
    controlHash = controlHash.hexdigest()
    if controlHash == Hash:
        if len(data["newPassword"]) > 7:
            if not bcrypt.check_password_hash(entry["password"],
                                          data["oldPassword"]):
                return "User validation failed", 401
            database_helper.change_password(bcrypt.generate_password_hash(
                    data["newPassword"]).decode("utf-8"), entry["token"])
            return "Password changed", 200
        return "Password should be longer than 7 characters", 422
    return "hash mismatch", 401


@app.route("/get_user_data_by_token/<email>/<Hash>")
def get_user_data_by_token(email, Hash):
    entry = database_helper.get_account_by_email(email)
    if not entry:
        return "User not found", 404
    controlHash = sha512()
    controlHash.update(bytes(entry["token"] + email + "GET", "utf-8"))
    controlHash = controlHash.hexdigest()
    if controlHash == Hash:
        entry = dict(entry)
        del entry["password"], entry["messages"], entry["videos"]
        del entry["images"], entry["token"]
        return jsonify(entry)
    return "User validation failed", 401


@app.route("/get_user_data_by_email/<email>/<oemail>/<Hash>")
def get_user_data_by_email(email, oemail, Hash):
    entry = database_helper.get_account_by_email(email)
    if not entry:
        return "User validation failed", 401
    controlHash = sha512()
    controlHash.update(bytes(entry["token"] + email + oemail + "GET", "utf-8"))
    controlHash = controlHash.hexdigest()
    if (controlHash == Hash):
        data = database_helper.get_account_by_email(oemail)
        if not data:
            return "User not found", 404
        data = dict(data)
        del data["password"], data["messages"], data["token"], data["videos"]
        del data["images"]
        return jsonify(data)
    return "hash mismatch", 401


# For next 3 functions the format of the stored messages is:
# data := <message> <messageCont>*
# <messageCont> := \x00 <message>
# <message> := <writer> \x01 <content>

@app.route("/get_user_messages_by_token/<email>/<Hash>")
def get_user_messages_by_token(email, Hash):
    entry = database_helper.get_account_by_email(email)
    if not entry:
        return "User validation failed", 401
    controlHash = sha512()
    controlHash.update(bytes(
        entry["token"] + email + "GET" + "/get_user_messages_by_token",
        "utf-8"))
    controlHash = controlHash.hexdigest()
    if controlHash == Hash:
        if entry["messages"]:
            data = [{"writer": dat.split(chr(1))[0],
                     "content": dat.split(chr(1))[1]}
                    for dat in entry["messages"].split(chr(0))]
        else:
            data = ""
        return jsonify(data)
    return "hash mismatch", 401


@app.route("/get_user_messages_by_email/<requesterEmail>/<email>/<Hash>")
def get_user_messages_by_email(requesterEmail, email, Hash):
    requester = database_helper.get_account_by_email(requesterEmail)
    if not requester:
        return "User validation failed", 401
    controlHash = sha512()
    controlHash.update(
        bytes(requester["token"] + requesterEmail + email, "utf-8"))
    controlHash = controlHash.hexdigest()
    if controlHash == Hash:
        data = database_helper.get_account_by_email(email)
        if not data:
            return "Invalid e-mail", 404
        if data["messages"]:
            data = [{"writer": dat.split(chr(1))[0],
                     "content": dat.split(chr(1))[1]}
                    for dat in data["messages"].split(chr(0))]
        else:
            data = ""
        return jsonify(data)
    return "hash mismatch", 401


@app.route("/post_message", methods=["POST", "GET"])
def post_message():
    data = request.get_json()
    email = data["email"]
    Hash = data["hash"]
    sender = database_helper.get_account_by_email(data["senderEmail"])
    if not sender:
        return "User not found", 404
    controlHash = sha512()
    controlHash.update(bytes(
        sender["token"] + sender["email"] + email + data["message"], "utf-8"))
    controlHash = controlHash.hexdigest()
    if controlHash == Hash:
        receiver = database_helper.get_account_by_email(email)
        if not receiver:
            return "Recipient not found", 404
        if receiver["messages"]:
            msg = receiver["messages"] + chr(0) + sender["email"] + chr(1) + \
            data["message"]
        else:
            msg = sender["email"] + chr(1) + data["message"]
        database_helper.add_message(msg, data["email"])
        return "Message posted", 200
    return "Hash mismatch", 401


# For next 3 functions the format of the stored medias (images and videos) is:
# data := <media> <mediaCont>*
# <mediaCont> := \x00 <media>
# <media> := <writer> \x01 <content>

@app.route("/get_media_list/<email>/<oemail>/<Hash>")
def get_media_list(email, oemail, Hash):
    sender = database_helper.get_account_by_email(email)
    if not sender:
        return "User validation failed", 401
    controlHash = sha512()
    controlHash.update(bytes(sender["token"] + email + oemail, "utf-8"))
    controlHash = controlHash.hexdigest()
    if controlHash == Hash:
        target = database_helper.get_account_by_email(oemail)
        if not target:
            return "User not found", 404
        data = {"images": target["images"],
                "videos": target["videos"]}
        if data["images"]:
            data["images"] = [{img.split(chr(1))[0]: img.split(chr(1))[1]} \
                              for img in data["images"].split(chr(0))]
        else:
            data["images"] = ""
        if data["videos"]:
            data["videos"] = [{vid.split(chr(1))[0]: vid.split(chr(1))[1]} \
                              for vid in data["videos"].split(chr(0))]
        else:
            data["videos"] = ""
        return jsonify(data)
    return "Hash mismatch", 401


@app.route("/upload_media/<email>/<oemail>/<Hash>", methods=["POST"],)
def upload_media(email, oemail, Hash):
    sender = database_helper.get_account_by_email(email)
    if not sender:
        return "User validation failed", 401
    controlHash = sha512()
    controlHash.update(bytes(sender["token"] + email + oemail, "utf-8"))
    controlHash = controlHash.hexdigest()
    if controlHash == Hash:
        if 'media' not in request.files:
            return "No media found", 404
        file = request.files["media"]
        if file.filename:
            destination = allowed_media(file.filename)
            if not destination:
                return "File is not a supported media", 401
            if file:
                filename = secure_filename(file.filename)
                receiver = database_helper.get_account_by_email(oemail)
                if not receiver:
                    return "Recipient not found", 404
                if receiver[destination]:
                    data = receiver[destination] + chr(0) + sender["email"]\
                           + chr(1) + filename
                else:
                    data = sender["email"] + chr(1) + filename
                file.save(os.path.join(app.config['UPLOAD_FOLDER'],
                                       filename))
                if destination == "images":
                    database_helper.add_image(data, receiver["email"])
                if destination == "videos":
                    database_helper.add_video(data, receiver["email"])
                return "File uploaded", 200
            return "File is unaddressable", 401
        return "No file selected", 401
    return "Hash mismatch", 401


ALLOWED_IMAGE_EXTENSIONS = {'png', 'jpg', 'jpeg', 'gif'}
ALLOWED_VIDEO_EXTENSIONS = {"mpeg", "mp4", "mkv"}


def allowed_media(filename):
    """
    Return the media type of the argument 'filename' if not supported return false

    Returns "images", "videos", or False
    """
    if '.' in filename and \
            filename.rsplit('.', 1)[1].lower() in \
            ALLOWED_IMAGE_EXTENSIONS:
        return "images"
    elif '.' in filename and \
            filename.rsplit('.', 1)[1].lower() in \
            ALLOWED_VIDEO_EXTENSIONS:
        return "videos"
    else:
        return False


@app.route("/get_media/<email>/<filename>/<Hash>", methods=["GET"])
def get_media(email, filename, Hash):
    sender = database_helper.get_account_by_email(email)
    if not sender:
        return "User validation failed", 401
    controlHash = sha512()
    controlHash.update(bytes(sender["token"] + email + filename, "utf-8"))
    controlHash = controlHash.hexdigest()
    if controlHash == Hash:
        return send_from_directory(app.config["UPLOAD_FOLDER"], filename)
    return "Hash mismatch", 401


@app.route("/get_member_count")
def get_member_count():
    return jsonify((len(TwidderSessions), database_helper.user_count()))


def update_graph():
    # For live data presentation, the changes in the number of
    # signed users is notified to the all clients sends the
    # number of signed in users and total members signed up
    update_ws()
    for sckt in TwidderSessions:
        TwidderSessions[sckt].send(json.dumps(
            (len(TwidderSessions), database_helper.user_count())))


def update_ws():
    """Delete closed sockets"""
    # Get a list of closed sockets
    topop = list()
    for sckt in TwidderSessions:
        if TwidderSessions[sckt].closed:
            topop.append(sckt)
    # If exists any, remove closed sockets
    if topop:
        for sckt in topop:
            TwidderSessions.pop(sckt)


_weight = [ord(i) for i in ascii_letters]


def token_generator():
    length = 70
    return "".join(choices(ascii_letters, weights=_weight, k=length))


if __name__ == "__main__":
    HttpServer = WSGIServer(('', 5000),
                            app,
                            handler_class=WebSocketHandler).serve_forever()
